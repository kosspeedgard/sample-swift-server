import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

let server = HTTPServer()
server.serverPort = 8080

var routes = Routes()

func returnJSON(message: String, response: HTTPResponse) {
    do {
        try response.setBody(json: ["message": message])
            .setHeader(.contentType, value: "application/json")
            .completed()
    }
    catch {
        response.setBody(string: "Error handler request: \(error.localizedDescription)")
            .completed()
    }
}

routes.add(method: .get, uri: "/test", handler: { (request, response) in
    returnJSON(message: "TEST API SUCCESS!!!", response: response)
})

routes.add(method: .get, uri: "/test/{num}", handler: { (request, response) in
    guard
        let num = request.urlVariables["num"],
        let numInt = Int(num)
    else {
        return
    }
    
    returnJSON(message: "You number is: \(numInt)", response: response)
})

routes.add(method: .post, uri: "/testPost", handler: { (request, response) in
    guard
        let json = try? request.postBodyString?.jsonDecode() as! [String: Any],
        let name = json["name"] as? String
    else {
        response.completed(status: .badRequest)
        return
    }

    returnJSON(message: "Your name is: \(name)", response: response)
})

server.addRoutes(routes)


do {
    try server.start()
}
catch {
    print(error.localizedDescription)
}
